import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  @Input() id;
  students;
  teacher;

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getStudents(this.id).subscribe((data) => {
      this.students = data;
    })

    this.dataService.getRoom(this.id).subscribe((data: any) => {
      if (data) {
        console.log('dataaaaaaaaaaaaaaaaaaaaaaaaa room: ', data);
        this.teacher = data.teacher;
      }
    })
  }

}
