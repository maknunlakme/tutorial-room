import {Component, OnInit} from '@angular/core';
import {TimestampService} from './services/timestamp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'tutorial-room';
  time: string;

  constructor(private timestampService: TimestampService
  ) {
  }


  ngOnInit(): void {
    this.timestampService.getTime().subscribe((data) => {
      if (data) {
        this.time = '' + data.toString();
      }
    });
  }
}
