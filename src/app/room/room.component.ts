import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../services/data.service";
import {log} from "util";

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit, OnDestroy {
  id
  safeURL;
  videoURL;
  username;
  role;
  timestamp;
  roomSubscription;

  constructor(
    private _sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.setGetUserName();
    this.route.params.subscribe((data) => {
      this.id = data['id'];
      this.checkTeacher();
      this.checkVideo();
    });
  }

  addNewMember() {
    const getSubscription = this.dataService
      .getRoom(this.id)
      .subscribe((value) => {
        if (value == undefined) {
          this.role = 'Teacher';
          this.dataService.createRoom(this.id, {teacher: this.username});
          this.checkVideo();
        } else {
          this.role = 'Student';
          this.dataService.createStudent(this.id, this.username, {student: this.username});
          this.checkVideo();
        }
        getSubscription.unsubscribe();
      });
  }


  checkTeacher() {
    this.roomSubscription = this.dataService.getRoom(this.id).subscribe((data: any) => {
      console.log('blaaaaaaah: ', data);
      if (data) {
        if (this.username != data.teacher) {
          const subscription = this.dataService.getStudent(this.id, this.username).subscribe((value) => {
            console.log('blah blah ', value);
            if (!value) {
                this.addNewMember();
            } else {
              this.role = 'Student';
              this.checkVideo();
            }
            subscription.unsubscribe();
          });
        } else {
          this.role = 'Teacher';
          this.checkVideo();
        }
      } else {
          this.addNewMember();
      }
    });
  }

  setGetUserName() {
    this.username = localStorage.getItem('username');
    if (!this.username) {
      this.username = prompt("Please tell me your name?");
      localStorage.setItem('username', this.username);
    }
  }

  checkVideo() {
    if (this.role == 'Student') {
      this.videoURL = 'https://www.youtube.com/embed/' + this.id + '?autoplay=1&mute=1&controls=0&t=' + this.timestamp;
    } else {
      this.videoURL = 'https://www.youtube.com/embed/' + this.id;
    }
    this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
  }

  ngOnDestroy() {

  }

  getCheckedValue(event: boolean) {
    if (event == true) {
      if (this.role == 'Student') {
        const subscription = this.dataService.getRoom(this.id).subscribe((data: any) => {
          const currentTeacher = data.teacher;
          this.dataService.createStudent(this.id, currentTeacher, {student: currentTeacher});
          subscription.unsubscribe();
        });
        this.role = 'Teacher';
        this.dataService.updateRoom(this.id, {teacher: this.username});
        this.dataService.deleteStudent(this.id, this.username);
        this.checkVideo();
      }
    } else {
      if (this.role == 'Teacher') {
        const subscription = this.dataService.getStudents(this.id).subscribe((data) => {
          console.log('dataaaaaaaaaaaaaaaaaaaaaaa ', data, data.length);
          if (data.length != 0) {
            this.role = 'Student';
            this.dataService.createStudent(this.id, this.username, {student: this.username});
            // this.dataService.deleteTeacher(this.id);
            this.dataService.updateRoom(this.id, {teacher: data[0].student});
            this.dataService.deleteStudent(this.id, data[0].student);
          } else {
            alert('you cannot be a student');
          }
          subscription.unsubscribe();
        });
        this.checkVideo();
      }
    }
  }

  logout() {
    this.roomSubscription.unsubscribe();
    if (this.role == 'Teacher') {
      console.log('callling???');
      const subscription = this.dataService.getStudents(this.id).subscribe((data) => {
        if (data.length != 0) {
          const name = data[0].student;
          this.dataService.deleteStudent(this.id, name)
            .then((res) => {
              console.log("res student: ", res);
            }).catch((err) => {
            console.log('err student', err);
          });
          this.dataService.updateRoom(this.id, {teacher: name});
        } else {
          console.log('blah blah blah: ', data.length, data);
          this.dataService.deleteRoom(this.id).then((res) => {
            console.log("res teacher: ", res);
          }).catch((err) => {
            console.log('err teacher', err);
          })
        }
        subscription.unsubscribe();
      });
    } else {
      this.dataService.deleteStudent(this.id, this.username);
    }
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
