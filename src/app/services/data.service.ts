import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
// import firebase from "firebase";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private db: AngularFirestore) {
  }

  getAllRooms() {
    return this.db.collection('rooms').valueChanges();
  }

  getRoom(id) {
    return this.db.collection('rooms').doc(id).valueChanges();
  }

  createRoom(id, room) {
    return this.db.collection('rooms').doc(id).set(room);
  }

  updateRoom(id, room) {
    return this.db.collection('rooms').doc(id).update(room);
  }

  deleteRoom(id) {
    console.log('id here delete:  ', id);
    return this.db.collection('rooms').doc(id).delete();
  }

  createStudent(id, name, student) {
    return this.db.collection('rooms').doc(id).collection('student').doc(name).set(student);
  }

  deleteStudent(id, name) {
    return this.db.collection('rooms').doc(id).collection('student').doc(name).delete();
  }

  // deleteTeacher(id) {
  //   return this.db.collection('rooms').doc(id).update({
  //     teacher: firebase.firestore.FieldValue.delete()
  //   });
  // }

  getStudents(id) {
    return this.db.collection('rooms').doc(id).collection('student').valueChanges();
  }

  getStudent(id, name) {
    return this.db.collection('rooms').doc(id).collection('student').doc(name).valueChanges();
  }
}
