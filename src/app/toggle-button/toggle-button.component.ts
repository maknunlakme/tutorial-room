import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.scss']
})
export class ToggleButtonComponent implements OnInit {
  @Input() id;
  @Input() role;
  @Output() changed = new EventEmitter<boolean>();
  buttonDisable=false;
  students;
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.getStudents(this.id).subscribe((data) => {
      this.students=data;
      console.log('what is going on??? ', data, this.role);
    })
  }

  checkButton() {
    if(this.students.length==0) {
      this.buttonDisable=true;
    } else {
      this.buttonDisable=false;
    }
  }

  changeCheckValue(event) {
    this.changed.emit(event.target.checked);
  }
}
