// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCmDgiCtRq6cOyQgowXzgUgpa04O7Y1SdE",
    authDomain: "video-room-83805.firebaseapp.com",
    projectId: "video-room-83805",
    storageBucket: "video-room-83805.appspot.com",
    messagingSenderId: "530012216361",
    appId: "1:530012216361:web:c1ba9a54d604d2315e9038",
    measurementId: "G-98902NGGRM",
    vapidKey: "BFd8VfFaq0l8cKDg5rvwTdjN54ke58Qf1zw-7dDquvcS9eJ95wowfuFF0mbjbQxxmNguxhUyAkhGmGTmcXy9Mfo"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
