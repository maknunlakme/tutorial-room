import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimestampService {
  time: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() {
  }

  getTime(): Observable<any> {
    return this.time.asObservable();
  }

  setTime(newValue): void {
    this.time.next(newValue);
  }
}
