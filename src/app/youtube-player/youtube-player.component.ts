import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {TimestampService} from '../services/timestamp.service';

@Component({
  selector: 'app-youtube-player',
  templateUrl: './youtube-player.component.html',
  styleUrls: ['./youtube-player.component.scss']
})
export class YoutubePlayerComponent implements OnInit {
  /* 1. Some required variables which will be used by YT API*/
  public YT: any;
  public video: any;
  public player: any;
  public reframe = false;
  ytValue = 'YT';
  time;
  isRestricted = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

  constructor(private timestampService: TimestampService) {
  }

  ngOnInit(): void {
    this.video = 'EY6cOlFPkpU';
    this.init();
  }

  init(): void {
    // Return if Player is already created
    if (window[this.ytValue]) {
      this.startVideo();
      return;
    }

    const tag = document.createElement('script');
    tag.src = 'http://www.youtube.com/iframe_api';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    /* 3. startVideo() will create an <iframe> (and YouTube player) after the API code downloads. */
    const blah = 'onYouTubeIframeAPIReady';
    window[blah] = () => this.startVideo();
  }

  startVideo(): void {
    this.reframe = false;
    this.player = new window[this.ytValue].Player('player', {
      videoId: this.video,
      playerVars: {
        autoplay: 1,
        modestbranding: 1,
        controls: 1,
        disablekb: 1,
        rel: 0,
        showinfo: 0,
        fs: 0,
        playsinline: 1
      },
      events: {
        onStateChange: this.onPlayerStateChange.bind(this),
        onError: this.onPlayerError.bind(this),
        onReady: this.onPlayerReady.bind(this),
      }
    });
  }

  /* 4. It will be called when the Video Player is ready */
  onPlayerReady(event): void {
    if (this.isRestricted) {
      event.target.mute();
      event.target.playVideo();
    } else {
      event.target.playVideo();
    }
  }

  /* 5. API will call this function when Player State changes like PLAYING, PAUSED, ENDED */
  onPlayerStateChange(event): void {
    console.log('player status change: ', event);
    switch (event.data) {
      case window[this.ytValue].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          console.log('started ' + this.cleanTime());
        } else {
          console.log('playing ' + this.cleanTime());
        }
        break;
      case window[this.ytValue].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          console.log('paused' + ' @ ' + this.cleanTime());
        }
        break;
      case window[this.ytValue].PlayerState.ENDED:
        console.log('ended ');
        break;
    }
  }

  cleanTime(): any {
    this.timestampService.setTime(Math.round(this.player.getCurrentTime()));
    console.log('time:::: ', this.time);
    return Math.round(this.player.getCurrentTime());
  }

  onPlayerError(event): void {
    switch (event.data) {
      case 2:
        console.log('' + this.video);
        break;
      case 100:
        break;
      case 101 || 150:
        break;
    }
  }
}
